use ::actix::prelude::*;
use ::actix::dev::{MessageResponse, ResponseChannel};
use ::actix::utils::IntervalFunc;
use bytes::{BufMut, BytesMut};
use log::{debug, error};
use tokio::net::tcp::TcpStream;

use std::collections::{HashMap, HashSet};
use std::net::SocketAddr;
use std::time::{Duration, SystemTime};

use crate::util::codec::Package;
use crate::actor::client::{ClientPackage, ClientStatus};
use crate::actor::tcpclient::TCPClient;

#[derive(Message)]
pub struct NewTCPConnection(pub TcpStream, pub SocketAddr);

pub struct Stats(pub usize, pub usize);

pub struct StatsRequest;

impl Message for StatsRequest {
    type Result = Stats;
}

impl<A, M> MessageResponse<A, M> for Stats
    where
        A: Actor,
        M: Message<Result = Stats>,
{
    fn handle<R: ResponseChannel<M>>(self, _: &mut A::Context, tx: Option<R>) {
        if let Some(tx) = tx {
            tx.send(self);
        }
    }
}

pub struct Server {
    sessions: HashSet<Recipient<Package>>,
    channels: HashMap<BytesMut, HashSet<Recipient<Package>>>,
    ping_interval: u64,
}

impl Default for Server {
    fn default() -> Server {
        Server {
            sessions: HashSet::new(),
            channels: HashMap::new(),
            ping_interval: 60,
        }
    }
}

impl Server {
    pub fn new(ping_interval: u64) -> Server {
        Server {
            sessions: HashSet::new(),
            channels: HashMap::new(),
            ping_interval,
        }
    }

    fn ping_tick(&mut self, _: &mut Context<Self>) {
        let mut bytes = BytesMut::with_capacity(8);
        bytes.put_u64_be(SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap_or(Duration::from_secs(1)).as_secs());
        let ping = Package::Ping(bytes);
        for session in self.sessions.iter() {
            match session.do_send(ping.clone()) {
                Err(e) => error!("[ERROR] Message send failed! {:?}", e),
                _ => {}
            }
        }
    }

    fn subscribe(&mut self, id: BytesMut, address: Recipient<Package>) {
        if !self.channels.contains_key(&id) {
            self.channels.insert(id.clone(), HashSet::new());
        }
        let set = self.channels.get_mut(&id).unwrap();
        if !set.contains(&address) {
            set.insert(address);
        }
        debug!("New subscription to {:?}. Total: {} subscriptions.", id, set.len());
    }

    fn unsubscribe(&mut self, id: BytesMut, address: Recipient<Package>) {
        if self.channels.contains_key(&id) {
            self.channels.get_mut(&id).unwrap().remove(&address);
            let len = self.channels.get(&id).unwrap().len();
            if len == 0 { self.channels.remove(&id); }
            debug!("Someone unsubscribed from {:?}. Total: {} subscriptions.", id, len);
        } else {
            debug!("Someone tried to unsubscribe from {:?}. Total: 0 subscriptions.", id);
        }
    }

    fn unsubscribe_from_all(&mut self, address: &Recipient<Package>) {
        self.channels.retain(|_, channel| {
            if channel.contains(address) { channel.remove(address); }
            !channel.is_empty()
        });
    }
}

impl Actor for Server {
    type Context = Context<Self>;

    fn started(&mut self, context: &mut Self::Context) {
        IntervalFunc::new(Duration::from_secs(self.ping_interval), Self::ping_tick)
            .finish().spawn(context);
    }
}

impl Handler<NewTCPConnection> for Server {
    type Result = ();

    fn handle(&mut self, connection: NewTCPConnection, ctx: &mut Self::Context) {
        let server_addr = ctx.address();
        TCPClient::new(connection, server_addr);
    }
}

impl Handler<ClientStatus> for Server {
    type Result = ();

    fn handle(&mut self, status: ClientStatus, _: &mut Self::Context) {
        match status {
            ClientStatus::Started(addr) => {
                self.sessions.insert(addr);
                debug!("New session was created. Total: {} sessions.", self.sessions.len());
            }
            ClientStatus::Stopped(addr) => {
                self.sessions.remove(&addr);
                self.unsubscribe_from_all(&addr);
                debug!("One session was stopped. Total: {} sessions.", self.sessions.len());
            }
        }
    }
}

impl Handler<ClientPackage> for Server {
    type Result = ();

    fn handle(&mut self, msg: ClientPackage, _: &mut Self::Context) {
        match msg.package {
            Package::Message(id, message) => {
                if self.channels.contains_key(&id) {
                    for address in self.channels.get(&id).unwrap().iter() {
                        if *address != msg.address {
                            match address.do_send(Package::Message(id.clone(), message.clone())) {
                                Err(e) => error!("[ERROR] Message send failed! {:?}", e),
                                _ => {}
                            }
                        }
                    }
                }
                debug!("Message to {:?}: {:?}", id, message);
            }
            Package::Subscribe(id) => {
                self.subscribe(id, msg.address);
            }
            Package::Unsubscribe(id) => {
                self.unsubscribe(id, msg.address);
            }
            // these packages must be processed in the Client actors
            Package::Ping(_) => {}
            Package::Pong(_) => {}
        }
    }
}

impl Handler<StatsRequest> for Server {
    type Result = Stats;

    fn handle(&mut self, _:StatsRequest, _: &mut Self::Context) -> Self::Result {
        Stats(self.channels.len(), self.sessions.len())
    }
}
