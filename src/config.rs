use std::fs;
use std::io::{Error, ErrorKind};
use std::path::Path;
use std::string::ToString;

use log::{info, warn, error};

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub tcp: TcpConfig,
    pub web: WebConfig,
    pub general: GeneralConfig,
}

#[derive(Serialize, Deserialize)]
pub struct TcpConfig {
    pub host: String,
    pub port: u16,
}

#[derive(Serialize, Deserialize)]
pub struct WebConfig {
    pub host: String,
    pub port: u16,
}

#[derive(Serialize, Deserialize)]
pub struct GeneralConfig {
    pub ping_interval: u64,
}

impl Config {
    fn default() -> Config {
        Config {
            tcp: TcpConfig { host: "127.0.0.1".to_string(), port: 5733 },
            web: WebConfig { host: "127.0.0.1".to_string(), port: 5780 },
            general: GeneralConfig { ping_interval: 60 },
        }
    }

    fn create_config_file<P: AsRef<Path>>(path: P, conf: &Config) -> anyhow::Result<()> {
        let str = toml::to_string(conf)?;
        fs::write(path, str)?;
        Ok(())
    }

    fn default_and_create<P: AsRef<Path>>(path: P) -> Config {
        let conf = Config::default();
        if let Err(e) = Config::create_config_file(path, &conf) {
            warn!("Could not encode defaut TOML config!\n{:?}", e)
        }
        conf
    }

    fn load_file<P: AsRef<Path>>(path: P) -> anyhow::Result<Config> {
        if path.as_ref().exists() {
            let str = fs::read_to_string(path)?;
            Ok(toml::from_str::<Config>(&str)?)
        } else {
            Err(anyhow::Error::new(Error::new(ErrorKind::NotFound, "Config file was not found.")))
        }
    }

    pub fn load<P: AsRef<Path>>(path: P) -> Config {
        match Config::load_file(&path) {
            Ok(conf) => conf,
            Err(e) => {
                info!("Correct configuration file not found, using default values.");
                error!("{}", e);
                Config::default_and_create(&path)
            }
        }
    }
}
