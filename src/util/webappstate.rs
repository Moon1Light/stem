use ::actix::Addr;
use crate::actor::server::Server;

pub struct WebAppState {
    pub template: tera::Tera,
    pub server: Addr<Server>,
}
