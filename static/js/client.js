// WebSocket client for STEM
// 2019, MIT (c) Totoro

'use strict';

// init
// --------------------------------------------------------------------------------- //
var url = new URL(window.location.href);
var CHANNEL = url.searchParams.get("id");
var MAX_LOG_SIZE = 1000;
var ADD_TIMESTAMPS = true;
var RENDER_CONTENT = url.searchParams.get("render") == "true";
var ONE_TIME_MESSAGE = url.searchParams.get("message");

document.title = "STEM: #" + CHANNEL;

// helpers
// --------------------------------------------------------------------------------- //
var utf8encoder = new TextEncoder('utf-8');
var utf8decoder = new TextDecoder('utf-8');

function encode(str) {
  return utf8encoder.encode(str);
}

function decode(array) {
  return utf8decoder.decode(array);
}

// network
// --------------------------------------------------------------------------------- //
var socket;

var pkg = {
  message: 0,
  subscribe: 1,
  unsubscribe: 2,
  ping: 3,
  pong: 4
}

function setSize(array, size) {
  array[0] = Math.floor(size / 256);
  array[1] = size % 256;
}

function setType(array, type) {
  array[2] = type;
}

function setIDLength(array, length) {
  array[3] = length;
}

function setID(array, encodedId) {
  array.set(encodedId, 4);
}

function packageMessage(id, message) {
  var encodedId = encode(id);
  var idLen = encodedId.length;
  var encodedMessage = encode(message);
  var size = 1 + 1 + idLen + encodedMessage.length;
  var array = new Uint8Array(size + 2);
  setSize(array, size);
  setType(array, pkg.message);
  setIDLength(array, idLen);
  setID(array, encodedId);
  var offset = 2 + 1 + 1 + idLen;
  array.set(encodedMessage, offset);
  return array;
}

function packageSubscribe(id) {
  var encodedId = encode(id);
  var idLen = encodedId.length;
  var size = 1 + 1 + idLen;
  var array = new Uint8Array(size + 2);
  setSize(array, size);
  setType(array, pkg.subscribe);
  setIDLength(array, idLen);
  setID(array, encodedId);
  return array;
}

var pingContent = new Uint8Array(256);
window.crypto.getRandomValues(pingContent);
function packagePing() {
  var size = 1 + pingContent.length;
  var array = new Uint8Array(size + 2);
  setSize(array, size);
  setType(array, pkg.ping);
  array.set(pingContent, 3);
  return array;
}

function packagePong(content) {
  var size = 1 + content.length;
  var array = new Uint8Array(size + 2);
  setSize(array, size);
  setType(array, pkg.pong);
  array.set(content, 3);
  return array;
}

var channelEncoded = encode(CHANNEL);
var channelLen = channelEncoded.length;
function parseMessagePackage(array) {
  return decode(array.slice(2 + 1 + 1 + channelLen, array.length));
}

function subscribeOnSocketEvents() {
  socket.onmessage = function (event) {
    if (event.data instanceof ArrayBuffer) {
      var array = new Uint8Array(event.data);
      if (array[2] == pkg.message) {
        var message = parseMessagePackage(array);
        if (RENDER_CONTENT) {
          renderContent(message);
        } else {
          appendToLog(message);
        }
      } else if (array[2] == pkg.ping) {
        // keep the connection alive by responding to the ping messages
        socket.send(packagePong(array.slice(3, array.length)));
      }
    }
  };
  socket.onopen = function() {
    socket.send(packageSubscribe(CHANNEL));
    log.style.background = "#333";
    console.log("Succesfully connected.");
    if (ONE_TIME_MESSAGE != null) {
      sendMessage(ONE_TIME_MESSAGE);
      console.log("One time message \"" + ONE_TIME_MESSAGE + "\" was sent to \"" + CHANNEL + "\" channel.");
    }
  };
  socket.onclose = function() {
    reconnect();
  }
  socket.onerror = function() {
    reconnect();
  }
}

function reconnect() {
  log.style.background = "rgb(128, 27, 59)";
  console.log("Reconnecting...");
  connect();
}

function connect() {
  var wsUrl = "wss://" + window.location.host + "/ws";
  socket = new WebSocket(wsUrl);
  socket.binaryType = "arraybuffer";
  subscribeOnSocketEvents();
}

// ui
// --------------------------------------------------------------------------------- //
function renderContent(message) {
  eval(message);
}

var log = document.getElementById("log");

function appendToLog(message, my) {
  var size = log.childElementCount;
  if (size >= MAX_LOG_SIZE - 1) {
    log.removeChild(log.childNodes[0]);
  }
  var isScrolledDown = (log.scrollHeight - log.offsetHeight - log.scrollTop) < 20;
  var item = document.createElement("pre");
  item.textContent = message;
  if (my) item.classList.add("my");
  if (ADD_TIMESTAMPS) {
    var timestamp = document.createElement("span");
    timestamp.textContent = "[ " + new Date().toLocaleTimeString('en-US', { hour12: false }) + " ]";
    timestamp.classList.add("timestamp");
    item.classList.add("content");
    var container = document.createElement("div");
    container.appendChild(timestamp);
    container.appendChild(item);
    item = container;
  }
  log.appendChild(item);
  if (isScrolledDown) log.scrollTop = log.scrollHeight;
}

var message = document.getElementById("message");
var send = document.getElementById("send");

function sendMessage(str) {
  var text = str || message.value;
  message.value = "";
  if (!RENDER_CONTENT) appendToLog(text, true);
  socket.send(packageMessage(CHANNEL, text));
}

message.addEventListener("keyup", function(event) {
  if (event.key == "Enter") {
    sendMessage();
  }
});

send.addEventListener("click", function(event) {
  sendMessage();
  message.focus();
});

// go
// --------------------------------------------------------------------------------- //
connect();
